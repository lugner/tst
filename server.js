// server.js
// load the things we need
var express = require('express');
var mysql = require('mysql');
var app = express();


var con = mysql.createConnection({
  host: "localhost",
  user: "testuser",
  password: "password",
  database: "Baereninfo"
});

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.post('/', function (req, res) {
  res.redirect('/spendeerfolgreich');
  ;
});

// set the view engine to ejs
app.set('view engine', 'ejs');

// add static asset folderapp.get('/'
app.use(express.static('public'));

// index page 
app.get('/', function (req, res) {
  res.render('pages/index');
});


// startseite
app.get('/startseite', function (req, res) {
  res.render('pages/startseite', { text: 'hallo' });
});


//baerenarten
app.get('/baerenarten', function (req, res) {

  con.connect(function (err) {
    var sql = "SELECT * FROM baereninfo ";
    con.query(sql, [], function (err, result) {
      var baereninfo = [];
      for (i = 0; i < result.length; i++) {
        baereninfo.push(result[i])

      }
      if (err) {
        console.log(err);
      }
      res.render('pages/baerenarten', { baereninfo });
    });
  });
});


// spenden

app.get('/spenden', function (req, res) {
  res.render('pages/spenden');
});


app.post('/spenden', function (req, res) {
  var f = 0;
  f = parseFloat(req.body.betrag);
  var ergebnis = 0;
  if (req.body.Vorname == "") {
    ergebnis = 0;
  }
  else if (req.body.Nachname == "") {
    ergebnis = 0;
  }
  else if (req.body.betrag == "") {
    ergebnis = 0;
  }
  else if (req.body.IBAN == "") {
    ergebnis = 0;
  }

  else if (req.body.IBAN.indexOf("DE") == -1) {
    ergebnis = 0;
  }

  else if (f <= 0) {
    ergebnis = 0;
  }
  else {
    ergebnis = 1;
  }
  if (ergebnis == 1) {
    con.connect(function (err) {
      var sql = "INSERT INTO spenden (Vorname, Nachname, betrag, IBAN, baerenart) VALUES ?";
      var values =
        [
          [req.body.Vorname, req.body.Nachname, req.body.betrag, req.body.IBAN, req.body.baerenart]
        ]
      con.query(sql, [values], function (err, result) {
        if (err) throw err;
        console.log("1 record inserted");
        res.redirect('/spendeerfolgreich');
      });
    });
  }
  else {
    res.redirect('back');
  }
});


//spendeerfolgreich
app.get('/spendeerfolgreich', function (req, res) {
  res.render('pages/spendeerfolgreich');
});
// kontakt
app.get('/kontakt', function (req, res) {
  res.render('pages/kontakt', { test: [{ text: 'text11' }, { text: 'text12' }] })
});

//amerikanischerschwarzbaer
app.get('/amerikanischerschwarzbaer', function (req, res) {
  res.render('pages/amerikanischerschwarzbaer');
});
//test
app.get('/tst', function (req, res) {
  res.render('pages/tst');
});
app.get('/api/baeren', function (req, res) {
  res.send(baerenservice.getbaeren());
});

app.listen(8080);
console.log('8080 is the magic port');